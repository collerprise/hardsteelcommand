Time sheet
==========

```
|------------+-------+-------+----------+-------------------------------------------------------------|
| DATE       | START |  STOP | SUBTOTAL | DESCRIPTION                                                 |
|------------+------:+------:+---------:+-------------------------------------------------------------|
| 2015-10-03 | 14:18 | 14:50 |    00:32 | creating the repository, setting up project                 |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-03 | 14:52 | 15:58 |    01:06 | making the database class and setting up                    |
|            |       |       |          | dependencies and javadoc.                                   |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-04 | 13:02 | 13:49 |    00:47 | more documentation on database class                        |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-04 | 13:52 | 14:25 |    00:33 | adding Item class and making javadoc for the class          |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-05 | 14:33 | 15:44 |    01:18 | adding date and status class, changing datamembers          |
|            |       |       |          | in Item to use status and date                              |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-10 | 12:04 | 12:53 |    00:49 | starting on ItemDAO, research to ui, adding                 |
|            |       |       |          | stringtodate method on date class                           |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-10 | 13:00 | 15:07 |    02:07 | making extra methods for searching the items table          |
|            |       |       |          | and adding update method for items, adding javadoc          |
|            |       |       |          | to status class and itemdao, fixing javadoc from            |
|            |       |       |          | previous commits                                            |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-10 | 17:13 | 18:24 |    01:11 | making the beginnings of the first ui: AddItemFrame         |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-11 | 11:19 | 13:23 |    02:04 | making delete operation for itemdao, adding the             |
|            |       |       |          | ability to save an item from the gui to the database        |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-13 | 16:57 | 17:24 |    00:27 | tweaking system of "stay open" to clean fields when         |
|            |       |       |          | a valid item has been entered                               |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-13 | 22:37 | 23:03 |    00:26 | fixing repository connection problem                        |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-20 | 12:00 | 13:02 |    01:02 | finalizing the frame class and starting the build of        |
|            |       |       |          | the ListItemFrame class to display a table with info        |
|            |       |       |          | part 1                                                      |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2015-10-20 | 13:10 | 13:34 |    00:24 | making the beginnings of the ListItemFrame and checking     |
|            |       |       |          | how to work with tables in java swing                       |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2016-01-29 | 14:36 | 16:20 |    01:44 | making changes to database code for closing the connection  |
|            |       |       |          | , adding table to the form for checking lists of items,     |
|            |       |       |          | first step to connecting add item frame to head frame       |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2016-01-30 | 15:56 | 17:36 |    01:40 | changing layout of additemframe, adding add item operation  |
|            |       |       |          | to list item frame, refresh of the table                    |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2016-01-31 | 10:47 | 12:33 |    01:46 | added 'show archived items' feature, added delete operation |
|            |       |       |          | from the table                                              |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2016-01-31 | 16:20 | 18:37 |    02:17 | splitting ui from actionlisteners on additem frame, other   |
|            |       |       |          | stuff i cannot remember                                     |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2016-02-01 | 13:03 | 16:04 |    03:01 | making CRUD update operation (detailitem frame), javadoc,   |
|            |       |       |          | planning on what to do for dataloss prevention              |
|------------+-------+-------+----------+-------------------------------------------------------------|
| 2016-02-01 | 16:54 | 17:37 |    00:43 | implementing data loss prevetion by locking & unlocking the |
|            |       |       |          | form                                                        |
|------------+-------+-------+----------+-------------------------------------------------------------|
| total      |       |       |    25:34 |                                                             |
|------------+-------+-------+----------+-------------------------------------------------------------|
```

> vim: set tw=0
