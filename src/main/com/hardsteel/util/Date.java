package com.hardsteel.util;

/**
 * Date object for abstracting the date of which a item became in the possetion
 * of the person managing this app.
 *
 * @author Nicolas.
 * @version 2.0.
 */
public class Date {
	/** The day of the month from 1 to 31. */
	private byte day;

	/** The month of the year from 1 to 12. */
	private byte month;

	/** The year, beginning at 1900 til 10 000. */
	private short year;

	/**
	 * Default constructor with default values for the day, month and year.
	 * <pre>{@code day=1, month=1, year=2015}</pre>
	 */
	public Date() {
		this(1, 1, 2015);
	}

	/**
	 * The constructor with day, month and year values to set the members of the
	 * class to these values.
	 *
	 * @param d the day of the month: <pre>{@code 1 - 31}</pre>
	 * @param m the month of the year: <pre>{@code 1 - 12}</pre>
	 * @param y the year: <pre>{@code 1900 - 10 000}</pre>
	 */
	public Date(int d, int m, int y) {
		setDay(d);
		setMonth(m);
		setYear(y);
	}

	/**
	 * This method will set the day of the date, it will constrain the value of
	 * this.day between 1 and 31, if the day parameter is above or below, the value
	 * 1 will be inserted.
	 *
	 * @param day the value which this.day will be set to.
	 */
	public void setDay(int day) {
		if (day >= 1 && day <= 31) {
			this.day = (byte)day;
		} else {
			this.day = 1;
		}
	}

	/**
	 * This method will set the month of the date, it will constrain the value of
	 * this.month between 1 and 12, if the month parameter is above or below, the
	 * value 1 will be inserted.
	 *
	 * @param month the value which this.month will be set to.
	 */
	public void setMonth(int month) {
		if (month >= 1 && month <= 12) {
			this.month = (byte)month;
		} else {
			this.month = 1;
		}
	}

	/**
	 * This method will set the year of the date, it will constrain the value of
	 * this.year between 1900 and 10 000, if the year parameter is above or below
	 * the value 2015 will be inserted.
	 *
	 * @param year the value which this.year wil be set to.
	 */
	public void setYear(int year) {
		if (year > 1900 && year < 10000) {
			this.year = (short)year;
		} else {
			this.year = 2015;
		}
	}

	/**
	 * This method will take a correct date string and parse it into a date object
	 * that can be used in the program, the correct format for the string is:
	 * <pre>{@code DD-MM-YYYY}</pre>
	 * there is a overloaded method to use a different delimiter.
	 *
	 * @param date a date string to be converted to a Date object.
	 * @return a Date object or null if the string format was incorrect.
	 */
	public static Date stringToDate(String date) {
		return Date.stringToDate(date, "-");
	}

	/**
	 * This method will take a correct date string and parse it into a date object
	 * that can be used in the program, the correct format for the string is:
	 * <pre>{@code DD-MM-YYYY}</pre>
	 * but the "-" can be a different delimiter
	 *
	 * @param date a date string to be converted to a Date object.
	 * @param delimiter a single character that is used as the delimiter between
	 * day month and year parts of the date string.
	 * @return a Date object or null if the string format was incorrect.
	 */
	public static Date stringToDate(String date, String delimiter) {
		if (date.length() == 10) {
			// correct date notation.
			String[] split = date.split(delimiter);
			if (split.length == 3) {
				return new Date(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
			}
		}
		return null;
	}

	/**
	 * This function will convert the date object to a will formatted string,
	 * the formatting is done by the bin-norms
	 * the format of the returned string is:
	 * <pre>{@code 02-04-2015 }</pre>
	 *
	 * @return bin-norm formatted string representing the date.
	 */
	public String toString() {
		return String.format("%02d", day) + "-" + String.format("%02d", month) + "-" + year;
		// will output:
		// 02-04-2012
	}
}
