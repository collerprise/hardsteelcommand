package com.hardsteel.util;

/**
 * Enum to describe the current status of the item.
 *
 * @author Nicolas
 * @version 3.3
 */
public enum Status {
	UNKNOWN(0, "Unknown"),
	AVAILABLE(1, "Available"),
	USED(2, "Used"),
	BROKEN(3, "Broken"),
	GONE(4, "Gone"),
	REPAIR(5, "Being repaired");

	/** The id of the status */
	private int id;

	/** The display name of the enum (for better readability) */
	private String displayName;

	/**
	 * The constructor of the Status enum.
	 *
	 * @param id the id of the enum.
	 * @param displayName the name that is being display to the user
	 */
	private Status(int id, String displayName) {
		this.id = id;
		this.displayName = displayName;
	}

	/**
	 * The getter of the id of the enum.
	 *
	 * @return id of the status class.
	 */
	public int getID() {
		return id;
	}

	/**
	 * The getter for the display name of the enum.
	 *
	 * @return the display name of the enum (for better readability).
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * This method will make an array out of the enum values, the names will be used
	 * <b>not</b> the display names.
	 * 
	 * @return a string array with all the names of the enums
	 */
	public static String[] toArray() {
		Status[] s = values();
		String[] ns = new String[s.length];

		for (int i = 0; i < s.length; i++) {
			ns[i] = s[i].name();
		}

		return ns;
	}

	/**
	 * This method will create a nice string to show what the status is.
	 *
	 * @return a formatted string with info about the value of the enum call upon.
	 */
	public String toDisplayString() {
		return "Status: " + displayName + " (" + id  + ")";
	}
}
