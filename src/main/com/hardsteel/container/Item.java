package com.hardsteel.container;

import com.hardsteel.util.Status;
import com.hardsteel.util.Date;

/**
 * This class "Item" is a container class and represents an piece of computer hardware,
 * it has all the data stored for the properties of the component.
 *
 * @author Nicolas
 * @version 1.0
 */
public class Item {
	/** The name of the item, used to identify the item */
	private String name;

	/** The name of the manufacturer of the item */
	private String brand;

	/**
	 * The data of aquisition of the item (also the date from which the waranty
	 * countdown starts).
	 */
	private Date date;

	/** The current status of the item (in use, being repaired, in stock, ...) */
	private Status status;

	/**
	 * An array with the names of the fields if needed to be placed inside the
	 * headers of a table.
	 */
	public static final String[] FIELDS = {"name", "brand", "date", "status"};

	/**
	 * Default constructor of the object witch sets nothing,
	 * to preven long constructor calls
	 */
	public Item() {
		this("", "", new Date(01, 01, 2015), Status.UNKNOWN);
	}

	/**
	 * Constructor to make the object with direct initialization of all the member
	 * variables.
	 *
	 * @param name The name of the item, for identification.
	 * @param brand The name of the manufacturer of the item.
	 * @param date The date of aquisition of the item.
	 * @param status The status of the item.
	 */
	public Item(String name, String brand, Date date, Status status) {
		this.name = name;
		this.brand = brand;
		this.date = date;
		this.status = status;
	}

	/**
	 * Constructor to copy one item's field values into another item for
	 * cloning type puposeses.
	 *
	 * @param item the item where the values of it's fields need to be copied
	 * into this new item.
	 */
	public Item(Item item) {
		this.name = item.getName();
		this.brand = item.getBrand();
		this.date = item.getDate();
		this.status = item.getStatus();
	}

	/**
	 * This method will copy all the information of this object's fields into
	 * the specified item object.
	 *
	 * @param item the item that needs to recieve all the information of the
	 * fields from this item.
	 */
	public void copy(Item item) {
		item.setName(this.name);
		item.setBrand(this.brand);
		item.setDate(this.date);
		item.setStatus(this.status);
	}

	/**
	 * Makes of the item in question an array of simple objects to be displayed
	 * in the table when listing the items.
	 *
	 * @return an object array with the information of the item.
	 */
	public Object[] toArray() {
		return new Object[] {name, brand, date.toString(), status};
	}

	/**
	 * Setter of the name member.
	 *
	 * @param name The name of the object.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Setter of the brand member.
	 *
	 * @param brand Name of the brand of the item.
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * Setter of the data aquisition.
	 *
	 * @param date The date of aquisition of the item.
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Setter of the status member.
	 *
	 * @param status The status of the item.
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * Getter for the name of the item.
	 *
	 * @return a String object as the name of the item.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Getter for the name of the manufacturer of the item.
	 *
	 * @return a String object as the name of the manufacturer of the item.
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * Getter for the date of aquisition of the item.
	 *
	 * @return a String object as the date of aquisition of the item.
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Getter for the status of the item.
	 *
	 * @return a String object as the status of the item.
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * This method will generate a string with all the datamember included.
	 *
	 * @return a String object as a list of all datamembers for this object.
	 */
	public String toString() {
		return "Item " + name + " {" + brand + ", " + date + ", " + status + "};";
	}
}
