package com.hardsteel.base;

import com.hardsteel.database.Database;
import com.hardsteel.ui.AddItemFrame;
import com.hardsteel.ui.ListItemFrame;

/**
 * This is the main class of this project, the class contains the main method
 * which starts of the application.
 *
 * @author Nicolas
 * @version 1.2
 */
public class HardsteelCommand {
	/**
	 * The main constructor of this class.
	 */
	public HardsteelCommand() {
		checkdb();
		//AddItemFrame frame = new AddItemFrame(500, 300);
		ListItemFrame frame = new ListItemFrame(500, 300);
	}

	/**
	 * The main method of the application, here everything starts
	 *
	 * @param args the command line argument passed to the app when it is invoked
	 * in the terminal:
	 * <pre>{@code java -jar hardsteelcommander username password}</pre>
	 * here username and password are the 2 arguments of the application and passed
	 * in a string array.
	 */
	public static void main(String[] args) {
		new HardsteelCommand();
	}

	/**
	 * This function opens and uses the database for the first time, it will check
	 * of the file exists and make the tables if needed then it will close the database
	 * for next use.
	 */
	private void checkdb() {
		// open a connection to the database (create a db file if it does not
		// exists yet.
		Database.openDatabase(Database.dbFile);

		// create the tables if they do not yet exist.
		Database.makeTables();

		// close the database connection.
		Database.closeDatabase();
	}
}
