package com.hardsteel.database.dao;

import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

import com.hardsteel.util.Status;
import com.hardsteel.util.Date;
import com.hardsteel.container.Item;
import com.hardsteel.database.Database;

/**
 * ItemDAO class is used to access the individual tables in the database, every
 * method in this class is static will do a specific thing: adding an item to the
 * items table, removing, updating and reading data from the database.
 *
 * @author Nicolas
 * @version 3.2
 */
public abstract class ItemDAO {
	/**
	 * This method will read all the items in the items table and return every
	 * single record back as an item object array.
	 *
	 * @return an item object array constructed from all the items in the database.
	 */
	public static List<Item> readAll() {
		List<Item> list = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM items;");

			while (rs.next()) {
				Item item = new Item();
				item.setBrand(rs.getString("brand"));
				item.setName(rs.getString("name"));
				item.setDate(Date.stringToDate(rs.getString("date")));
				item.setStatus(Status.valueOf(rs.getString("status")));

				list.add(item);
			}

			s.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}

		return list;
	}

	/**
	 * This method will read all the items in the items table and return every
	 * single record back as an item object array.
	 *
	 * @return an item object array constructed from all the items in the database.
	 */
	public static List<Item> readAllExisting() {
		List<Item> list = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM items WHERE status != 'GONE';");

			while (rs.next()) {
				Item item = new Item();
				item.setBrand(rs.getString("brand"));
				item.setName(rs.getString("name"));
				item.setDate(Date.stringToDate(rs.getString("date")));
				item.setStatus(Status.valueOf(rs.getString("status")));

				list.add(item);
			}

			s.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}

		return list;
	}


	/**
	 * This method will look up every item in the items table that have the correct
	 * namepart, the name is being wildly used, this means you can use a part of
	 * a name to find the correct name.
	 *
	 * @param namepart a string as the part of the name, based on this part all
	 * items with a name that contains this part will be returned.
	 * @return an item array with all the items that have the namepart in there name.
	 */
	public static List<Item> readByName(String namepart) {
		List<Item> list = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM items WHERE name LIKE '%" + namepart + "%';");

			while (rs.next()) {
				Item item = new Item();
				item.setBrand(rs.getString("brand"));
				item.setName(rs.getString("name"));
				item.setDate(Date.stringToDate(rs.getString("date")));
				item.setStatus(Status.valueOf(rs.getString("status")));

				list.add(item);
			}

			s.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}

		return list;
	}

	/**
	 * This method will look up every item in the items table that have the correct
	 * manufacturer's name, the name is being wildly used, this means you can use a part of
	 * a name to find the correct manufacturer name.
	 *
	 * @param brandpart a string as the part of the manufacturer's name, based on
	 * this part all items with a manufacturer's name that contains this part will
	 * be returned.
	 * @return an item array with all the items that have the namepart in there
	 * manufacturer's name.
	 */
	public static List<Item> readByBrand(String brandpart) {
		List<Item> list = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM items WHERE brand LIKE '%" + brandpart + "%';");

			while (rs.next()) {
				Item item = new Item();
				item.setBrand(rs.getString("brand"));
				item.setName(rs.getString("name"));
				item.setDate(Date.stringToDate(rs.getString("date")));
				item.setStatus(Status.valueOf(rs.getString("status")));

				list.add(item);
			}

			s.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}

		return list;
	}

	/**
	 * This method will look up every item in the items table that have the correct
	 * aquisition date.
	 *
	 * @param date a date object representing the date of aquisition.
	 * @return an item array with all the items that were aquired on that date
	 */
	public static List<Item> readByDate(Date date) {
		List<Item> list = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM items WHERE date = '" + date.toString() + "';");

			while (rs.next()) {
				Item item = new Item();
				item.setBrand(rs.getString("brand"));
				item.setName(rs.getString("name"));
				item.setDate(Date.stringToDate(rs.getString("date")));
				item.setStatus(Status.valueOf(rs.getString("status")));

				list.add(item);
			}

			s.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}

		return list;
	}

	/**
	 * This method will look up every item that has a certain status.
	 *
	 * @param status the status all the items must have in order to be returned.
	 * @return an item object array which holds all the items that have the specified
	 * status.
	 */
	public static List<Item> readByStatus(Status status) {
		List<Item> list = new ArrayList<>();

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			Statement s = null;

			s = c.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM items WHERE status = '" + status + "';");

			while (rs.next()) {
				Item item = new Item();
				item.setBrand(rs.getString("brand"));
				item.setName(rs.getString("name"));
				item.setDate(Date.stringToDate(rs.getString("date")));
				item.setStatus(Status.valueOf(rs.getString("status")));

				list.add(item);
			}

			s.close();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}

		return list;
	}

	/**
	 * This method will add an item to the items table.
	 *
	 * @param item the item object that will be added to the table.
	 * @return true if the item has been inserted successfully, false if the item
	 * with the name already exists.
	 */
	public static boolean addItem(Item item) {
		if (ItemDAO.readByName(item.getName()).size() != 0)
			return false;

		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "INSERT INTO items (name, brand, date, status) " +
				"VALUES ('" + item.getName() + "', '" + item.getBrand() + "', '" + item.getDate() + "', '" + item.getStatus() + "');";
			s.executeUpdate(sql);

			s.close();
			c.commit();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}
		return true;
	}

	/**
	 * This method will update the an item in the database that has been read first,
	 * then it will check the database for that item with all it's field and update
	 * them using a new updated item object with the correct field values.
	 *
	 * @param o the object that the database need to contain, this is the record that
	 * needs to be updated.
	 * @param n the new item object that holds the updated data that needs to be
	 * transferd to the database.
	 * @return true if the update has been completed succesfully, false if the update
	 * has failed, this can happen if the item is not found or is already updated.
	 */
	public static boolean update(Item o, Item n) {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "UPDATE items SET name = '" + n.getName() + "', brand = '" + n.getBrand() + "', date = '" + n.getDate().toString() + "', status = '" + n.getStatus() + "' " +
				"WHERE name = '" + o.getName() + "' AND brand = '" + o.getBrand() + "' AND date = '" + o.getDate().toString() + "' AND status = '" + o.getStatus() + "';";
			//System.out.println(sql);
			s.executeUpdate(sql);

			s.close();
			c.commit();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}
		return true;
	}

	/**
	 * This method will theoreticaly delete an item, it will set the item's status
	 * to "GONE", the item record does not get deleted so historic data is kept.
	 *
	 * @param i the item of which the status will be changed
	 * @return a boolean to show if the update completed successfully.
	 */
	public static boolean deleteItem(Item i) {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "UPDATE items SET status = '" + Status.GONE + "' " +
				"WHERE name = '" + i.getName() + "' AND brand = '" + i.getBrand() + "' AND date = '" + i.getDate().toString() + "' AND status = '" + i.getStatus() + "';";
			s.executeUpdate(sql);

			s.close();
			c.commit();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}
		return true;
	}

	/**
	 * This method will delete the item permanently from the database. It will
	 * not just set the status of the item to 'gone' but will remove the data
	 * intirely.
	 * 
	 * @param i the item to be deleted from the database.
	 * @return true if the item was found and successfully deleted, false if not.
	 */
	public static boolean purgeItem(Item i) {
		try {
			Connection c = Database.getConnection();
			if (c == null) {
				Database.openDatabase(Database.dbFile);
				c = Database.getConnection();
			}

			c.setAutoCommit(false);
			Statement s = null;

			s = c.createStatement();
			String sql = "DELETE FROM items WHERE name = '" + i.getName() +
				"' AND brand = '" + i.getBrand() +
				"' AND date = '" + i.getDate().toString() +
				"' AND status = '" + i.getStatus() + "';";
			s.executeUpdate(sql);

			s.close();
			c.commit();
		} catch (SQLException e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		} finally {
			Database.closeDatabase();
		}
		return true;
	}
}
