package com.hardsteel.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 * The database class is used by the whole project, it is responible for the only
 * database that can be connected to the application.
 *
 * @author Nicolas
 * @version 2.2
 */
public final class Database {
	/** connection object used to communicate with the database. */
	private static Connection c = null;

	/** The class from the sqlite library to connect to the dbfile. */
	public static final String dbPackage = "org.sqlite.JDBC";

	/**
	 * The path via the connection driver to the file: after ':' comes the path
	 * starting at the jar file being run.
	 */
	public static final String dbFilePath = "jdbc:sqlite:";

	/**
	 * The name of the database file if preseded with a slash seperated path it
	 * will be a path starting from the jar file being run to the file
	 * <pre> {@code "path/to/hardware.db" } </pre>
	 * it will place the file in the folder
	 * <pre>{@code 'jarfiledir'/path/to/ }</pre>
	 */
	public static final String dbFile = "hardware.db";

	/**
	 * This function will open a connection to the database by first loading the
	 * sqlite database driver, if the database file does not exist it will create
	 * one! otherwise it will lock the file to the process of the application.
	 *
	 * @param filename the name of the database file
	 */
	public static void openDatabase(String filename) {
		try {
			Class.forName(dbPackage);
			if (c == null) {
				c = DriverManager.getConnection(dbFilePath + dbFile);
				System.out.println("Opened database succesfully");
			}
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * This method will close the connection to the database file and clean up
	 * the variables in this class.
	 */
	public static void closeDatabase() {
		if (c != null) {
			try {
				c.close();
				c = null;
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				System.exit(1);
			}
		}
	}

	/**
	 * This function will be called to create the tables in the file, if they already
	 * exist nothing will happen sinds sqlite will throw an error saying
	 * <pre>{@code SQL Error: tabel "tablename" already exists.}</pre>
	 * every create table statement is placed in a seperate try catch block to make
	 * sure that when the error is thrown the other tables a tried to be created.
	 */
	public static void makeTables() {
		if (c != null) {
			try {
				Statement s = null;
				String sql = null;

				s = c.createStatement();

				sql = "CREATE TABLE items (\n" +
					"name CHAR(50) NOT NULL,\n" +
					"brand CHAR(20) NOT NULL,\n" +
					"date CHAR(10),\n" +
					"status CHAR(15)\n" +
					");";

				s.executeUpdate(sql);
				s.close();
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				//System.exit(1);
			}

			try {
				Statement s = null;
				String sql = null;

				s = c.createStatement();

				sql = "CREATE TABLE desktops (\n" +
					"power_supply VARCHAR(50),\n" +
					"mother_board VARCHAR(50),\n" +
					"cpu VARCHAR(50),\n" +
					"gpu VARCHAR(50),\n" +
					"ram_1 VARCHAR(50),\n" +
					"ram_2 VARCHAR(50),\n" +
					"ram_3 VARCHAR(50),\n" +
					"ram_4 VARCHAR(50),\n" +
					"tower VARCHAR(50),\n" +
					"os VARCHAR(50),\n" +
					"disk_1 VARCHAR(50),\n" +
					"disk_2 VARCHAR(50),\n" +
					"disk_3 VARCHAR(50),\n" +
					"disk_4 VARCHAR(50),\n" +
					"FOREIGN KEY (power_supply) REFERENCES items(name),\n" +
					"FOREIGN KEY (mother_board) REFERENCES items(name),\n" +
					"FOREIGN KEY (cpu) REFERENCES items(name),\n" +
					"FOREIGN KEY (gpu) REFERENCES items(name),\n" +
					"FOREIGN KEY (ram_1) REFERENCES items(name),\n" +
					"FOREIGN KEY (ram_2) REFERENCES items(name),\n" +
					"FOREIGN KEY (ram_3) REFERENCES items(name),\n" +
					"FOREIGN KEY (ram_4) REFERENCES items(name),\n" +
					"FOREIGN KEY (tower) REFERENCES items(name),\n" +
					"FOREIGN KEY (disk_1) REFERENCES items(name),\n" +
					"FOREIGN KEY (disk_2) REFERENCES items(name),\n" +
					"FOREIGN KEY (disk_3) REFERENCES items(name),\n" +
					"FOREIGN KEY (disk_4) REFERENCES items(name)\n" +
					");";
				s.executeUpdate(sql);
				s.close();
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				//System.exit(1);
			}

			try {
				Statement s = null;
				String sql = null;

				s = c.createStatement();

				sql = "CREATE TABLE laptops (\n" +
					"brand VARCHAR(20),\n" +
					"cpu VARCHAR(20),\n" +
					"gpu VARCHAR(20),\n" +
					"ram VARCHAR(50),\n" +
					"os VARCHAR(50),\n" +
					"disk_1 VARCHAR(50),\n" +
					"disk_2 VARCHAR(50),\n" +
					"FOREIGN KEY (disk_1) REFERENCES items(name),\n" +
					"FOREIGN KEY (disk_2) REFERENCES items(name)\n" +
					");";
				s.executeUpdate(sql);
				s.close();
			} catch (SQLException e) {
				System.err.println(e.getClass().getName() + ": " + e.getMessage());
				//System.exit(1);
			}
		} else {
			//System.out.println("not database connection has been set!");
			JOptionPane.showMessageDialog(null, "No database connection!\nFirst connect to a database.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Get the connection object of this class.
	 *
	 * @return the connection object with the data to the database file.
	 */
	public static Connection getConnection() {
		return c;
	}
}
