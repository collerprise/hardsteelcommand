package com.hardsteel.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.SpringLayout;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import java.util.HashMap;

import com.hardsteel.container.Item;
import com.hardsteel.database.dao.ItemDAO;
import com.hardsteel.reference.WindowTitles;
import com.hardsteel.util.Date;
import com.hardsteel.util.Status;
import com.hardsteel.ui.event.SaveItemEvent;

/**
 * The AddItemFrame frame represents a window of the application that will
 * have all the fields and buttons to let a user add items with all their
 * information to the database.
 */
@SuppressWarnings("serial")
public class AddItemFrame extends Frame {
	private static String[] COMBO_SATUSES = Status.toArray();

	private JPanel pButtons;
	private JPanel pInputs;

	private JButton bReset;
	private JButton bClose;
	private JButton bSave;

	private JLabel[] lNames;

	public static final String TXT_NAME = "Name";
	public static final String TXT_BRAND = "Brand";
	public static final String TXT_DATE = "Date";
	public static final String CB_STATUS = "Status";
	public static final String CHB_KEEP_OPEN = "Keep open";

	private HashMap<String, JComponent> components;

	/**
	 * Main constructor for the window <br>
	 * sets all the basic stuff for a swing window.
	 *
	 * @param width set the width of the window.
	 * @param height set the height of the window.
	 */
	public AddItemFrame(int width, int height) {
		super(width, height, WindowTitles.ADD_ITEM_TITLE);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addItems();
		this.setVisible(true);
	}

	/**
	 * Add the widgets to the form using the add method.
	 */
	@Override
	protected void addItems() {
		initObjects();

		this.setLayout(new BorderLayout());
		pButtons.add(components.get(CHB_KEEP_OPEN));
		pButtons.add(bReset);
		pButtons.add(bClose);
		pButtons.add(bSave);

		SpringLayout spring = new SpringLayout();

		pInputs.setLayout(spring);

		pInputs.add(lNames[0]);
		pInputs.add(lNames[1]);
		pInputs.add(lNames[2]);
		pInputs.add(lNames[3]);
		pInputs.add(components.get(TXT_NAME));
		pInputs.add(components.get(TXT_BRAND));
		pInputs.add(components.get(TXT_DATE));
		pInputs.add(components.get(CB_STATUS));

		// vertical
		spring.putConstraint(SpringLayout.NORTH, lNames[0], 10, SpringLayout.NORTH, pInputs);
		spring.putConstraint(SpringLayout.NORTH, components.get(TXT_NAME), 5, SpringLayout.SOUTH, lNames[0]);
		spring.putConstraint(SpringLayout.NORTH, lNames[1], 10, SpringLayout.SOUTH, components.get(TXT_NAME));
		spring.putConstraint(SpringLayout.NORTH, components.get(TXT_BRAND), 5, SpringLayout.SOUTH, lNames[1]);
		spring.putConstraint(SpringLayout.NORTH, lNames[2], 10, SpringLayout.SOUTH, components.get(TXT_BRAND));
		spring.putConstraint(SpringLayout.NORTH, components.get(TXT_DATE), 5, SpringLayout.SOUTH, lNames[2]);
		spring.putConstraint(SpringLayout.NORTH, lNames[3], 10, SpringLayout.SOUTH, components.get(TXT_DATE));
		spring.putConstraint(SpringLayout.NORTH, components.get(CB_STATUS), 5, SpringLayout.SOUTH, lNames[3]);

		//horizontal
		spring.putConstraint(SpringLayout.WEST, lNames[0], 10, SpringLayout.WEST, pInputs);
		spring.putConstraint(SpringLayout.WEST, lNames[1], 0, SpringLayout.WEST, lNames[0]);
		spring.putConstraint(SpringLayout.WEST, lNames[2], 0, SpringLayout.WEST, lNames[0]);
		spring.putConstraint(SpringLayout.WEST, lNames[3], 0, SpringLayout.WEST, lNames[0]);
		spring.putConstraint(SpringLayout.WEST, components.get(TXT_NAME), 0, SpringLayout.WEST, lNames[0]);
		spring.putConstraint(SpringLayout.WEST, components.get(TXT_BRAND), 0, SpringLayout.WEST, lNames[0]);
		spring.putConstraint(SpringLayout.WEST, components.get(TXT_DATE), 0, SpringLayout.WEST, lNames[0]);
		spring.putConstraint(SpringLayout.WEST, components.get(CB_STATUS), 0, SpringLayout.WEST, lNames[0]);
		spring.putConstraint(SpringLayout.EAST, components.get(TXT_NAME), -10, SpringLayout.EAST, pInputs);
		spring.putConstraint(SpringLayout.EAST, components.get(TXT_BRAND), -10, SpringLayout.EAST, pInputs);
		spring.putConstraint(SpringLayout.EAST, components.get(TXT_DATE), -10, SpringLayout.EAST, pInputs);
		spring.putConstraint(SpringLayout.EAST, components.get(CB_STATUS), -10, SpringLayout.EAST, pInputs);

		this.add(pInputs, BorderLayout.CENTER);
		this.add(pButtons, BorderLayout.SOUTH);
	}

	/**
	 * Initializes the object sush as buttons and the panels
	 * sets the arrays for the input forms.
	 */
	private void initObjects() {
		// initializing the buttons.
		bReset = new JButton("Reset");
		bClose = new JButton("Close");
		bSave = new JButton("Save");

		lNames = new JLabel[Item.FIELDS.length];
		for (int i = 0; i < Item.FIELDS.length; i++) {
			lNames[i] = new JLabel(Item.FIELDS[i] + ": ");
		}

		components = new HashMap<String, JComponent>();

		components.put(TXT_NAME, new JTextField());
		components.put(TXT_BRAND, new JTextField());
		components.put(TXT_DATE, new JTextField());
		components.put(CB_STATUS, new JComboBox());
		components.put(CHB_KEEP_OPEN, new JCheckBox("keep open?"));

		JComboBox statuses = (JComboBox)components.get(CB_STATUS);
		for (int i = 0; i < COMBO_SATUSES.length; i++) {
			statuses.addItem(COMBO_SATUSES[i]);
		}

		bReset.addActionListener(ae -> {
			this.resetForm();
		});

		bClose.addActionListener(ae -> {
			this.dispose();
		});

		bSave.addActionListener(new SaveItemEvent(this, components));

		pButtons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		pInputs = new JPanel();
	}

	public void showError(String fields) {
		JOptionPane.showMessageDialog(null, "The following fields are empty:" + fields, "Input error", JOptionPane.ERROR_MESSAGE);
	}

	public void resetForm() {
		((JTextField)components.get(TXT_NAME)).setText("");
		((JTextField)components.get(TXT_BRAND)).setText("");
		((JTextField)components.get(TXT_DATE)).setText("");
		((JComboBox)components.get(CB_STATUS)).setSelectedIndex(0);
	}
}
