package com.hardsteel.ui;

import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class StaticTableModel extends DefaultTableModel {
	public StaticTableModel() {
		super();
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
