package com.hardsteel.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.table.DefaultTableModel;

import com.hardsteel.container.Item;
import com.hardsteel.database.dao.ItemDAO;

/**
 * This class will handle the of updating the table on a form where a 
 * refresh of the values is needed, this is not the same event as the initial
 * loading of the table.
 */
public class RefreshTableEvent implements ActionListener {
	/** The model to manipulate the items of. */
	private DefaultTableModel model;

	/** Does the class need to take 'gone' items into a count?. */
	private JCheckBox useGone;

	/**
	 * The constructor of the event.
	 *
	 * @param model the model that holds the information for the {@code JTable}
	 * and will be used by the event to clear and repopulate with the new correct
	 * information.
	 * @param useGone this boolean indicates whether or not 'gone' items need
	 * to be included into the table model.
	 */
	public RefreshTableEvent(DefaultTableModel model, JCheckBox useGone) {
		if (model == null) {
			throw new NullPointerException();
		}
		this.model = model;
		this.useGone = useGone;
	}

	/**
	 * This method will do all the work of refreshing the data for the model,
	 * it will repopulate the model with the data from the database, if the 
	 * {@code useGone} boolean is set to {@code true} the data will also
	 * include {@code GONE} items into the table, if not not.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		int amount = model.getRowCount();
		for (int i = 0; i < amount; i++) {
			model.removeRow(0);
		}
		List<Item> items = null;
		if (useGone.isSelected()) {
			items = ItemDAO.readAll();
		} else {
			items = ItemDAO.readAllExisting();
		}
		for (int i = 0; i < items.size(); i++) {
			model.addRow(items.get(i).toArray());
		}
	}
}
