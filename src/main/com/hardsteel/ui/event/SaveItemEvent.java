package com.hardsteel.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import java.util.HashMap;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;

import com.hardsteel.container.Item;
import com.hardsteel.database.dao.ItemDAO;
import com.hardsteel.ui.AddItemFrame;
import com.hardsteel.util.Date;
import com.hardsteel.ui.Frame;
import com.hardsteel.util.Status;

public class SaveItemEvent implements ActionListener {
	private HashMap<String, JComponent> components;
	private AddItemFrame frame;

	public SaveItemEvent(AddItemFrame frame, HashMap<String, JComponent> components) {
		if (components == null) {
			throw new NullPointerException();
		}
		this.components = components;
		if (frame == null) {
			throw new NullPointerException();
		}
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Item item = new Item();

		String tmp = "";
		String fields = "";

		JTextField name = (JTextField)components.get(AddItemFrame.TXT_NAME);
		JTextField brand = (JTextField)components.get(AddItemFrame.TXT_BRAND);
		JTextField date = (JTextField)components.get(AddItemFrame.TXT_DATE);
		JComboBox status = (JComboBox)components.get(AddItemFrame.CB_STATUS);
		JCheckBox open = (JCheckBox)components.get(AddItemFrame.CHB_KEEP_OPEN);

		tmp = name.getText().trim();
		if (!tmp.isEmpty()) item.setName(tmp); else fields += "\n - Name";

		tmp = brand.getText().trim();
		if (!tmp.isEmpty()) item.setBrand(tmp); else fields += "\n - Brand";

		tmp = date.getText().trim();
		if (tmp != "" && Date.stringToDate(tmp) != null)
			item.setDate(Date.stringToDate(tmp));
		else
			fields += "\n - Date";

		item.setStatus(Status.valueOf((String)status.getSelectedItem()));

		if (fields == "")
			ItemDAO.addItem(item);
		else {
			frame.showError(fields);
			return;
		}

		if (!open.isSelected()) {
			frame.dispose();
		}
		frame.resetForm();
	}
}
