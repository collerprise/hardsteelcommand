package com.hardsteel.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;

import com.hardsteel.container.Item;
import com.hardsteel.database.dao.ItemDAO;
import com.hardsteel.util.Date;
import com.hardsteel.util.Status;
import com.hardsteel.ui.StaticTableModel;

/**
 * This class will handle the event of the deletion of the selected rows in the
 * tables.
 *
 * @author collerprise.
 */
public class DeleteTableEvent implements ActionListener {
	/** The model to manipulate the items of. */
	private JTable table;

	/**
	 * The main constructor for the event that takes the table to be modified.
	 *
	 * @param table the table which holds the data that needs to be manipulated
	 * when the event is fired.
	 */
	public DeleteTableEvent(JTable table) {
		if (table == null) {
			throw new NullPointerException();
		}
		this.table = table;
	}

	/**
	 * This method will perform the sequence to delete all the selected items
	 * from the table and in the database, the items are NOT deleted from the
	 * database but only their status is updated to GONE.
	 *
	 * @param e the event that was fired with all the information that goes
	 * with it.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		int[] rows = table.getSelectedRows();
		StaticTableModel model = (StaticTableModel)table.getModel();
		for (int i = 0; i < rows.length; i++) {
			Item item = new Item();
			item.setName((String)model.getValueAt(rows[i] - i, 0));
			item.setBrand((String)model.getValueAt(rows[i] - i, 1));
			item.setDate(Date.stringToDate((String)model.getValueAt(rows[i] - i, 2)));
			item.setStatus((Status)model.getValueAt(rows[i] - i, 3));
			ItemDAO.deleteItem(item);
			model.removeRow(rows[i] - i);
		}
	}
}
