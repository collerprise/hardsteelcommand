package com.hardsteel.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JCheckBox;

import com.hardsteel.container.Item;
import com.hardsteel.database.dao.ItemDAO;
import com.hardsteel.reference.WindowTitles;
import com.hardsteel.ui.Frame;
import com.hardsteel.ui.event.RefreshTableEvent;
import com.hardsteel.ui.event.DeleteTableEvent;
import com.hardsteel.ui.StaticTableModel;
import com.hardsteel.util.Date;
import com.hardsteel.util.Status;
import com.hardsteel.ui.DetailItemFrame;

/**
 * This frame is responcible for showing the data you want to see in a table like
 * view.
 *
 * @author Nicolas
 * @version 1.0
 */
@SuppressWarnings("serial")
public class ListItemFrame extends Frame {
	/** The content panel where the content of the frame is placed on. */
	private JPanel content;

	/** The "taskbar" for the application where all the functions can be found. */
	private JPanel control;

	/** The options panel for special options for the table. */
	private JPanel options;

	/** The table where all the info that is being looked up is shown */
	private JTable contentTable;

	/**
	 * This is the default constuctor for the ListItemFrame class, it calls the
	 * superconstructor.
	 *
	 * @param width the width of the frame
	 * @param height the heigth of the frame
	 */
	public ListItemFrame(int width, int height) {
		super(width, height, WindowTitles.LIST_ITEM_TITLE);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addItems();
		this.setVisible(true);
	}

	/**
	 * Method for initializing the objects on the frame and setting everything up
	 * for a good frame.
	 */
	@Override
	protected void addItems() {
		StaticTableModel model = new StaticTableModel();
		model.setColumnIdentifiers(Item.FIELDS);
		contentTable = new JTable();
		JScrollPane scroll = new JScrollPane(contentTable);

		this.setLayout(new BorderLayout());
		content = new JPanel(new BorderLayout());

		control = new JPanel();
		options = new JPanel(new FlowLayout(FlowLayout.LEFT));

		JCheckBox chbUseGone = new JCheckBox("include Archived");

		JButton btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new RefreshTableEvent(model, chbUseGone));

		JButton btnAdd = new JButton("Add Item");
		btnAdd.addActionListener(e -> {
			AddItemFrame frameAdd = new AddItemFrame(500, 300);
		});

		JButton btnDetails = new JButton("Details");
		btnDetails.addActionListener(e -> {
			int itemIndex = contentTable.getSelectedRow();
			if (itemIndex != -1) {
				Item item = new Item();
				item.setName((String)model.getValueAt(itemIndex, 0));
				item.setBrand((String)model.getValueAt(itemIndex, 1));
				item.setDate(Date.stringToDate((String)model.getValueAt(itemIndex, 2)));
				item.setStatus((Status)model.getValueAt(itemIndex, 3));
				DetailItemFrame frameDetail = new DetailItemFrame(500, 300, item);
			}
		});

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new DeleteTableEvent(contentTable));

		control.add(btnAdd);
		control.add(btnDetails);
		control.add(btnDelete);
		control.add(btnRefresh);
		
		options.add(chbUseGone);

		List<Item> items = ItemDAO.readAllExisting();

		contentTable.setModel(model);

		for (int i = 0; i < items.size(); i++) {
			model.addRow(items.get(i).toArray());
		}

		content.add(scroll, BorderLayout.CENTER);
		this.add(control, BorderLayout.NORTH);
		this.add(content, BorderLayout.CENTER);
		this.add(options, BorderLayout.SOUTH);
	}
}
