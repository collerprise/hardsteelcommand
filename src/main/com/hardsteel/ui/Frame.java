package com.hardsteel.ui;

import javax.swing.JFrame;

/**
 * This is an abstract template class for all the other frames, this class forces
 * all frames to have an addItems method and sets a default constructor.
 *
 * @author Nicolas
 * @version 1.0
 */
@SuppressWarnings("serial")
public abstract class Frame extends JFrame {
	/**
	 * Main constructor for the window <br>
	 * sets all the basic stuff for a swing window.
	 *
	 * @param width set the width of the window.
	 * @param height set the height of the window.
	 */
	public Frame(int width, int height, String title) {
		super(title);
		this.setSize(width, height);
		this.setLocationRelativeTo(null);
	}

	/**
	 * Force all child classes to have a method to initialize all the item on the
	 * frame.
	 */
	protected abstract void addItems();
}
