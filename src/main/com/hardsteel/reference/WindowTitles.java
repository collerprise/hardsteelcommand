package com.hardsteel.reference;

public abstract class WindowTitles {
	public static final String FRAME_PRE = "HardsteelCommand | ";
	public static final String ADD_ITEM_TITLE = FRAME_PRE + "Add Item";
	public static final String LIST_ITEM_TITLE = FRAME_PRE + "List Items";
	public static final String DETAILED_ITEM_TITLE = FRAME_PRE + "Detail Items";
}
